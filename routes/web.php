<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'AccueilController');
Route::resource('/profile', 'ProfileController');

Route::resource('/partner', 'PartnerController');

Auth::routes();

Route::resource('/register', 'RegisterController');
 
Route::get('/home', 'AccueilController@index')->name('home');

Route::get('/model', function(){
	return view('Pages.Model.index');
});
