<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['nom', 'prenom', 'age', 'adresse', 'poids', 'taille', 'teint', 'profession', 'user_id', 'sexe', 'about', 'nationalite', 'pays', 'profile', 'avatar', 'tel', 'email', 'religion', 'situation'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
