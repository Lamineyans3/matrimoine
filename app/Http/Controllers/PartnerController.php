<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Flashy;
use App\Partner;
use App\Profile;
class PartnerController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        $partner = Partner::where('user_id', Auth::user()->id)->first();
        return view('Pages.Partner.index', compact(['partner', 'profile']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'height' => 'required|string',
            'intervalle' => 'required|string',
            'sexe' => 'required|string',
            'marital' => 'required|string',
            'religion' => 'required|string',
            'caste' => 'required|string',
            'pays' => 'required|string',
            'teint' => 'required|string',
            'domaine' => 'required|string'
            ]);
        Partner::create([
            'height' => $request->height,
            'intervalle' => $request->intervalle,
            'sexe' => $request->sexe,
            'marital' => $request->marital,
            'religion' => $request->religion,
            'caste' => $request->caste,
            'pays' => $request->pays,
            'teint' => $request->teint,
            'domaine' => $request->domaine,
            'user_id' => Auth::user()->id
            ]);
        Flashy::message('Information enregistrer avec succès');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'height' => 'required|string',
            'intervalle' => 'required|string',
            'sexe' => 'required|string',
            'marital' => 'required|string',
            'religion' => 'required|string',
            'caste' => 'required|string',
            'pays' => 'required|string',
            'teint' => 'required|string',
            'domaine' => 'required|string'
            ]);
        $update = Partner::findOrFail($id);
        $update->update([
            'height' => $request->height,
            'intervalle' => $request->intervalle,
            'sexe' => $request->sexe,
            'marital' => $request->marital,
            'religion' => $request->religion,
            'caste' => $request->caste,
            'pays' => $request->pays,
            'teint' => $request->teint,
            'domaine' => $request->domaine,
            'user_id' => Auth::user()->id
            ]);
        Flashy::message('Information modifier avec succès');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
