<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Partner;
use Flashy;
use Auth;
use App\Http\Requests\ProfileFormRequest;
class ProfileController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->id;
        $profile = Profile::where('user_id', $user)->first(); 
        return view('Pages.UserProfile.index', compact(['profile']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileFormRequest $request)
    {
        $user = Auth::user()->id;
        $r =  rand(10,100) ;
        // $profile = Sexe+mysociety+user_id+random;
        $photo = time().'.'.$request->avatar->getClientOriginalExtension();
            $img = 'images/'.$photo;
         
            $insert = $request->avatar->move(public_path('images'), $img);

        $profile = strtoupper($request->sexe.'GWD'.$user.''.$r);

        Profile::create([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'age' => $request->age,
                'adresse' => $request->adresse,
                'poids' => $request->poids,
                'taille' => $request->taille,
                'teint' => $request->teint,
                'profession' => $request->profession,
                'sexe' => $request->sexe,
                'about' => $request->about,
                'nationalite' => $request->nationalite,
                'religion' => $request->religion,
                'situation' => $request->situation,
                'pays' => $request->pays,
                'profile' => $profile,
                'avatar' => $img,
                'tel' => $request->tel,
                'user_id' => $user,
                'email' => $request->email,
            ]);
        $profile_info = Profile::where('user_id', $user)->first();
        Flashy::message('Votre profile à été ajouter avec succès');
        return redirect(route('profile.show', $profile_info));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::findOrFail($id);
        return view('Pages.UserProfile.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->request([
            'nom' => 'required|string|max:20',
            'prenom' => 'required|string|max:20',
            'age' => 'required|date',
            'adresse' => 'required|string|max:20',
            'poids' => 'required|string|max:15',
            'taille' => 'required|string|max:15',
            'teint' => 'required|string|max:10',
            'profession' => 'required|string|max:90',
            'sexe' => 'required|string',
            'about' => 'required|string',
            'nationalite' => 'required|string|max:100',
            'pays' => 'required|string|max:50',
            'tel' => 'required|string|max:10',
            'religion' => 'required|string|max:20',
            'situation' => 'required|string|max:50',
            ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
