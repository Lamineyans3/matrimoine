<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|string|max:20',
            'prenom' => 'required|string|max:20',
            'age' => 'required|date',
            'adresse' => 'required|string|max:20',
            'poids' => 'required|string|max:15',
            'taille' => 'required|string|max:15',
            'teint' => 'required|string|max:10',
            'profession' => 'required|string|max:90',
            'sexe' => 'required|string',
            'about' => 'required|string',
            'nationalite' => 'required|string|max:100',
            'pays' => 'required|string|max:50',
            'tel' => 'required|string|max:10',
            'religion' => 'required|string|max:20',
            'situation' => 'required|string|max:50',
            'avatar' => 'required|image|mimes:jpg,JPG,jpeg,JPEG,png,PNG,GIF,gif',
        ];
    }
}
