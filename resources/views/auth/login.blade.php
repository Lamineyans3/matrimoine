@extends('Template.base', ['title' => "Accueil"])
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="login-w3ls"> <br><br>
                    <h3 class="text-center">Connecter vous</h3>
                    <form id="signin" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <label>Identifiant </label>
                        <input type="text" name="name" placeholder="Identifiant" required="">
                        <label>Password</label>
                        <input type="password" name="password" placeholder="Password" required="">  
                        <div class="w3ls-loginr"> 
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span>Remember Me?</span>
                         
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                        </div>
                        <div class="clearfix"> </div>
                        <input type="submit" value="Login">
                        <div class="clearfix"> </div>
                        <div class="social-icons">
                            <ul>  
                                <li><a href="#"><span class="icons"><i class="fa fa-facebook" aria-hidden="true"></i></span><span class="text">Facebook</span></a></li> 
                                <li class="twt"><a href="#"><span class="icons"><i class="fa fa-twitter" aria-hidden="true"></i></span><span class="text">Twitter</span></a></li>  
                            </ul> 
                            <div class="clearfix"> </div>
                        </div>  
                    </form>
                </div>
                
            </div>
        </div>
    </div> <br>
    <div class="agile-assisted-service text-center">
        <h4>Service Assistance</h4>
        <p>Our Relationship Managers have helped thousands of members find their life partners.</p>
        <a href="">Voir Plus</a>
    </div>
           
@stop