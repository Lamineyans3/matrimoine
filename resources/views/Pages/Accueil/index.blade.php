@extends('Template.base', ['title' => "Accueil"])

@section('content')
	<!-- section slide -->

	<div class="w3layouts-banner" id="home">
		<div class="container">
			<div class="logo">
				<h1><a class="cd-logo link link--takiri" href="/">{{ config('app.name')}} <span><i class="fa fa-heart" aria-hidden="true"></i>Âme Soeur.</span></a></h1>
			</div>
			<div class="clearfix"></div>
			<div class="agileits-register">
				@include('includs/info')
				<h3>Enregistrer vous maintenant!</h3>
				<form class="form-horizontal" method="POST" action="{{ route('register.store') }}">
					{{ csrf_field() }}
					<div class="w3_modal_body_grid w3_modal_body_grid1">
						<span>Identifiant:</span>
						<input type="text" name="name" placeholder="votre identifiant" value="{{ old('name') }}" required autofocus/>
					</div>
					<!-- <div class="w3_modal_body_grid w3_modal_body_grid1">
						<span>Date Of Birth:</span>
						<input class="date" id="datepicker" name="Text" type="text" value="mm/dd/yyyy" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '2/08/2013';}" required="" />
					</div> --> 
					<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>Mobile No:</span>
					<!-- country codes (ISO 3166) and Dial codes. -->
						<input id="phone" type="tel" name="contact" value="{{ old('contact') }}" required autofocus>
					  <!-- Load jQuery from CDN so can run demo immediately -->
					  <script src="{{ asset('js/intlTelInput.js')}} "></script>
					  <script>
						$("#phone").intlTelInput({
						  utilsScript: "{{ asset('js/utils.js') }}"
						});
					  </script>
					</div>
					<div class="w3_modal_body_grid">
						<span>Email:</span>
						<input type="email" name="email" placeholder="monemail@gmail.com"  value="{{ old('email') }}" required autofocus/>
					</div>
					<div class="w3_modal_body_grid w3_modal_body_grid1">
						<span>Password:</span>
						<input type="password" name="password" placeholder="**********" required=""/>
					</div>
					<div class="w3_modal_body_grid w3_modal_body_grid1">
						<span>Confirm Password:</span>
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
					</div>
					<div class="w3-agree">
						<input type="checkbox" id="c1" name="agree" value="1" required >
						<label class="agileits-agree">J'ai lu & accepte les <a href="terms.html">Termes et Conditions</a></label>
					</div>
					<input type="submit" value="Enregistrer" />
					<div class="clearfix"></div>
					<p class="w3ls-login">Déja membre? <a href="#" data-toggle="modal" data-target="#myModal">Connecter vous</a></p>
				</form>
			</div>
			<!-- Modal -->
			<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
			  <div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
					<h4 class="modal-title">Login to Continue</h4>
				  </div>
				  <div class="modal-body">
					<div class="login-w3ls">
						<form id="signin" method="POST" action="{{ route('login') }}">
							{{ csrf_field() }}
							<label>Identifiant </label>
							<input type="text" name="name" placeholder="Identifiant" required="">
							<label>Password</label>
							<input type="password" name="password" placeholder="Password" required="">	
							<div class="w3ls-loginr"> 
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span>Remember Me?</span>
                             
							<a class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
							</div>
							<div class="clearfix"> </div>
							<input type="submit" value="Login">
							<div class="clearfix"> </div>
							<div class="social-icons">
								<ul>  
									<li><a href="#"><span class="icons"><i class="fa fa-facebook" aria-hidden="true"></i></span><span class="text">Facebook</span></a></li> 
									<li class="twt"><a href="#"><span class="icons"><i class="fa fa-twitter" aria-hidden="true"></i></span><span class="text">Twitter</span></a></li>  
								</ul> 
								<div class="clearfix"> </div>
							</div>	
						</form>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		</div>
	</div>
	<!-- section comment sa marche -->
	<div class="w3l_find-soulmate text-center">
		<h3>Comment ça marche</h3>
		<div class="container">
			<a class="scroll" href="#home">
				<div class="col-md-3 w3_soulgrid">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					<h3>Inscription</h3>
					<p>Inscrivez-vous gratuitement et renseigner votre profile</p>
				</div>
			</a>
			<a class="scroll" href="#home">
				<div class="col-md-3 w3_soulgrid">
					<i class="fa fa-search" aria-hidden="true"></i>
					<h3>Récherche</h3>
					<p>Réchercher votre futur partenaire</p>
				</div>
			</a>
			<a class="scroll" href="#home">
				<div class="col-md-3 w3_soulgrid">
					<i class="fa fa-users" aria-hidden="true"></i>
					<h3>Connection</h3>
					<p>Connecter vous pour faciliter votre récherche</p>
				</div>
			</a>
			<a class="scroll" href="#home">
				<div class="col-md-3 w3_soulgrid">
					<i class="fa fa-comments-o" aria-hidden="true"></i>
					<h3>Inter Action</h3>
					<p>Rétrouver votre partenaire et commencer la conversation</p>
				</div>
			</a>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- section quelque profile -->
	<div class="w3layouts_featured-profiles">
		<div class="container">
			<div class="agile_featured-profiles">
				<h2>Quelques Profile</h2>
				<ul id="flexiselDemo3">
					<li>
						@if($profiles->isNotEmpty())
							@foreach($profiles as $profile)
								<div class="col-md-3 biseller-column">
									@auth
										<a href="{{ route('profile.show',$profile)}}">
											<div class="profile-image">
												<img src="{{config('app.url').''.$profile->avatar}}" class="img-responsive" alt="profile image" width="255" height="286">
												<div class="agile-overlay">
												<h4>Profile ID: {{$profile->profile}}</h4>
													<ul>
														<li><span>Age / Taille</span>: {{Date::make($profile->age)->until()}} / {{$profile->taille}}</li>
														<li><span>Poids</span>: {{$profile->poids}}</li>
														<li><span>Religion</span>: {{$profile->religion}}</li>
														<li><span>Profession</span>: {{$profile->profession}}</li>
														<li><span>Teint</span>: {{$profile->teint}}</li>
														<li><span>Situation</span>: {{$profile->situation}}</li>
													</ul>
												</div>
											</div>
										</a>

									@else
										<a href="#" data-toggle="modal" data-target="#myModal">
											<div class="profile-image">
												<img src="{{config('app.url').''.$profile->avatar}}" class="img-responsive" alt="profile image" >
												<div class="agile-overlay">
												<h4>Profile ID: {{$profile->profile}}</h4>
													<ul>
														<li><span>Age / Taille</span>: {{Date::make($profile->age)->until()}} / {{$profile->taille}}</li>
														<li><span>Poids</span>: {{$profile->poids}}</li>
														<li><span>Religion</span>: {{$profile->religion}}</li>
														<li><span>Profession</span>: {{$profile->profession}}</li>
														<li><span>Teint</span>: {{$profile->teint}}</li>
														<li><span>Situation</span>: {{$profile->situation}}</li>
													</ul>
												</div>
											</div>
										</a>
									@endauth
								</div>
							@endforeach
						@endif
					</li>
				</ul>
			</div>   
		</div>			
	</div> 
	<script type="text/javascript" src="js/jquery.flexisel.js"></script><!-- flexisel-js -->	
	<script type="text/javascript">
		 $(window).load(function() {
			$("#flexiselDemo3").flexisel({
				visibleItems:1,
				animationSpeed: 1000,
				autoPlay: false,
				autoPlaySpeed: 5000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems:1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems:1
					},
					tablet: { 
						changePoint:768,
						visibleItems:1
					}
				}
			});
			
		});
    </script>
	<!-- section assistance -->
	<div class="agile-assisted-service text-center">
		<h4>Service Assistance</h4>
		<p>Our Relationship Managers have helped thousands of members find their life partners.</p>
		<a href="">Voir Plus</a>
	</div>
@stop