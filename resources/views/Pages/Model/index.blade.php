<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
			<h4 class="modal-title">Login to Continue</h4>
		  </div>
		  <div class="modal-body">
			<div class="login-w3ls">
				<form id="signin" method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}
					<label>Identifiant </label>
					<input type="text" name="name" placeholder="Identifiant" required="">
					<label>Password</label>
					<input type="password" name="password" placeholder="Password" required="">	
					<div class="w3ls-loginr"> 
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span>Remember Me?</span>
                     
					<a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
					</div>
					<div class="clearfix"> </div>
					<input type="submit" value="Login">
					<div class="clearfix"> </div>
					<div class="social-icons">
						<ul>  
							<li><a href="#"><span class="icons"><i class="fa fa-facebook" aria-hidden="true"></i></span><span class="text">Facebook</span></a></li> 
							<li class="twt"><a href="#"><span class="icons"><i class="fa fa-twitter" aria-hidden="true"></i></span><span class="text">Twitter</span></a></li>  
						</ul> 
						<div class="clearfix"> </div>
					</div>	
				</form>
			</div>
		  </div>
		</div>
	</div>
</div>