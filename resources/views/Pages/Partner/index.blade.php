@extends('Template.base', ['title' => "Information de partenaire desiré"])

@section('content')
	<div class="feedback">
		<div class="container">
			<h2>Completer vos information</h2>
			<p>Renseigner les informations de votre partenaire desiré pour completer votre profil.</p>
			<form action="{{ $a = ($partner) ? route('partner.update',$partner) : route('partner.store') }}" method="post" enctype="multipart/form-data" files="true">
				{{csrf_field()}}
				@if($partner)
					{{method_field('PUT')}}
				@endif
				<div class="col-md-6">
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Âge:</label>
						@php $i = 20 @endphp
						<select name="intervalle" required="">
							@if($partner)
								<option value="{{ $partner->intervalle }}">{{ $partner->intervalle }}</option>
							@endif
							@for ($i=20, $j = 25; $i < 70, $j < 70; $i+=5, $j+=5)   
								<option value="{{ $i.' - '.$j.' ans' }}">{{ $i.' - '.$j.' ans' }}</option>
							@endfor
						</select>
					</div>
					
					<div class="agileits">
						<label>Poids:</label>
						<select id="" class="frm-field required" required="" name="height">
							@if($partner)
								<option value="{{ $partner->height }}">{{ $partner->height }}</option>
							@endif
							@for ($j=10; $j < 150; $j+=5)
								<option value="{{$j.' KG'}}">{{$j.' KG'}}</option>
							@endfor
						</select>
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Sexe:</label>
						<select id="" class="frm-field required" required="" name="sexe">  
							@if($partner)
								@if($partner->sexe == "m")
									<option value="m">Homme</option>   
									<option value="f">Femme</option>
								@else
									<option value="f">Femme</option>
									<option value="m">Homme</option>   
								@endif
							@else
							<option value="m">Homme</option>   
							<option value="f">Femme</option>
							@endif
							   
						</select>
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Taille  :</label>
						<select id="" class="frm-field required" required="" name="caste">
							@if($partner)
								<option value="{{ $partner->caste }}">{{ $partner->caste }}</option>
							@endif
							@for ($i=10; $i < 100; $i+=5)
								<option value="{{'1m'.$i}}">1m{{$i}}</option>
							@endfor
								<option value="2m">2m</option>
						</select>
					</div>
					
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Réligion  :</label>
						<select id="" class="frm-field required" required="" name="religion">
							@if($partner)
								<option value="{{ $partner->religion }}">{{ ucfirst(strtoupper( $partner->religion ))}}</option>
							@endif  
							<option value="musulman">Musulman</option>   
							<option value="catholique">Catholique</option>   
							<option value="protestant">Prostestant</option>   
							<option value="autre">Autres</option>   
						</select>
					</div>
				</div>

				<div class="col-md-6">

					<div class="agileits">
						<label>Profession:</label>
						<input type="text" placeholder="Profession" required="required" name="domaine" value="{{$partner->domaine ?? old('domaine')}}" />
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Teint  :</label>
						<select id="" class="frm-field required" required="" name="teint">
							@if($partner)
								<option value="{{ $partner->teint }}">{{ $partner->teint }}</option>
							@endif  
							<option value="Noir">Noir</option>   
							<option value="Clair">Claire</option>   
							<option value="Brun">Brun</option>   
						</select>
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Situation  :</label>
						<select id="" class="frm-field required" required="" name="marital"> 
							@if($partner)
								<option value="{{ $partner->marital }}">{{ $partner->marital }}</option>
							@endif 
							<option value="Celibataire">Célibataire</option>   
							<option value="Divorcer">Divorcer</option>   
							<option value="Veuve">Veuve</option>   
							<option value="Veufe">Veufe</option>   
						</select>
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Pays  :</label>
						<input type="text" required="required" name="pays" placeholder="Guinée" value="{{$partner->pays ?? old('pays')}}" />
					</div>

					
				</div>
				<div class="clearfix"></div>

				<div class="w3_submit">
					<input type="submit" value="Enregistrer"/>
				</div>
			</form>
		</div>
	</div>
	<!-- section assistance -->
	<div class="agile-assisted-service text-center">
		<h4>Service Assistance</h4>
		<p>Our Relationship Managers have helped thousands of members find their life partners.</p>
		<a href="">Voir Plus</a>
	</div>
@stop