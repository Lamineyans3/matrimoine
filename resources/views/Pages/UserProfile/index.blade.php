@extends('Template.base', ['title' => "Information Profile"])

@section('content')
	<div class="feedback">
		<div class="container">
			<h2>Renseigner votre profile</h2>
			<p>Renseigner tous les champs suivi (*) pour completer votre profil.</p>
			<form action="{{ $a= ($profile) ? route('profile.update',$profile) : route('profile.store') }}" method="post" enctype="multipart/form-data" files="true">
				{{csrf_field()}}
				@if($profile)
					{{method_field('PUT')}}
				@endif
				<div class="col-md-6">
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Nom:</label>
						<input type="text" placeholder="Nom de famille" name="nom" required="required" value="{{$profile->nom ?? old('nom')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Âge  :</label>
						<input type="date" required="required" name="age" value="{{$profile->age ?? old('age')}}"  />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Sexe:</label>
						<select id="" class="frm-field required" required="" name="sexe">  
							<option value="m">Homme</option>   
							<option value="f">Femme</option>   
						</select>
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Taille  :</label>
						<select id="" class="frm-field required" required="" name="taille">
							@for ($i=10; $i < 100; $i++)
								<option value="{{'1m'.$i}}">1m{{$i}}</option>
							@endfor
								<option value="2m">2m</option>
						</select>
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Profession  :</label>
						<input type="text" required="required" name="profession" placeholder="Informaticien" value="{{$profile->profession ?? old('profession')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Pays  :</label>
						<input type="text" required="required" name="pays" placeholder="Guinée" value="{{$profile->pays ?? old('pays')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Contact  :</label>
						<input type="text" required="required" name="tel" placeholder="660025241" value="{{$profile->tel ?? old('tel')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Réligion  :</label>
						<select id="" class="frm-field required" required="" name="religion">  
							<option value="musulman">Musulman</option>   
							<option value="catholique">Catholique</option>   
							<option value="protestant">Prostestant</option>   
							<option value="autre">Autres</option>   
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="agileits">
						<label>Prénom:</label>
						<input type="text" placeholder="Prénom" required="required" name="prenom" value="{{$profile->prenom ?? old('prenom')}}" />
					</div>
					<div class="agileits">
						<label>Adresse:</label>
						<input type="text" placeholder="Adresse" name="adresse" required="required" value="{{$profile->adresse ?? old('adresse')}}"/>
					</div>
					
					<div class="agileits">
						<label>Poids:</label>
						<select id="" class="frm-field required" required="" name="poids">
							@for ($j=10; $j < 150; $j++)
								<option value="{{$j.' KG'}}">{{$j.' KG'}}</option>
							@endfor
						</select>
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Teint  :</label>
						<select id="" class="frm-field required" required="" name="teint">  
							<option value="Noir">Noir</option>   
							<option value="Clair">Claire</option>   
							<option value="Brun">Brun</option>   
						</select>
					</div>

					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Nationalité  :</label>
						<input type="text" required="required" name="nationalite" placeholder="Guinéenne" value="{{$profile->nationalite ?? old('nationalite')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Photo  :</label>
						<input type="file" name="avatar" value="{{$profile->avatar ?? old('avatar')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;"></span>E-mail  :</label>
						<input type="text" name="email" placeholder="info@gmail.com" value="{{$profile->email ?? old('email')}}" />
					</div>
					<div class="agileits">
						<label><span style="color:red;font-weight: 100;">*</span>Situation  :</label>
						<select id="" class="frm-field required" required="" name="situation">  
							<option value="Marier">Marié</option>   
							<option value="Celibataire">Célibataire</option>   
							<option value="Divorcer">Divorcer</option>   
							<option value="Veuve">Veuve</option>   
							<option value="Veufe">Veufe</option>   
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 w3_feedbacktextmessage">
					<label><span style="color:red;font-weight: 100;">*</span>A Propos de vous:</label>
					<textarea name="about" placeholder=""> {{$profile->about ?? old('about')}} </textarea>
				</div>
				<div class="clearfix"></div>
				<div class="w3_submit">
					<input type="submit" value="Enregistrer"/>
				</div>
			</form>
		</div>
	</div>
	<!-- section assistance -->
	<div class="agile-assisted-service text-center">
		<h4>Service Assistance</h4>
		<p>Our Relationship Managers have helped thousands of members find their life partners.</p>
		<a href="">Voir Plus</a>
	</div>
@stop