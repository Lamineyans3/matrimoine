@extends('Template.base', ['title' => "Mon Profile"])

@section('content')
	<!-- information du profile -->
	<div class="w3ls-list">
		<div class="container">
		<h2>Profile de {{ $profile->nom.' '. $profile->prenom }}</h2>
		<div class="col-md-9 profiles-list-agileits">
			<div class="single_w3_profile">
				<div class="agileits_profile_image">
					<img src="{{asset($profile->avatar)}}" alt="{{$profile->nom}} image" width="260" height="280" />
				</div>
				<div class="w3layouts_details">
					<h4>Profile ID : {{$profile->profile}}</h4>
					<span>Compte créer il y a {{ Date::make($profile->created_at)->diffForHumans() }}.</span>
					<p>{{Date::make($profile->age)->until()}} , {{$profile->poids}}, {{$profile->taille}}, {{$a = ($profile->sexe == 'm') ? 'Homme' : 'Femme'}}, {{$profile->nationalite}} .</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="profile_w3layouts_details">
				<div class="agileits_aboutme">
					<div class="row">
						<div class="col-sm-12">
							<a class="btn btn-info" href="{{ route('profile.show',$profile) }}">Mon profile</a>
							<a class="btn btn-info" href="{{ url('/profile') }}">Modifier mon profile</a>
							<a class="btn btn-info" href="{{ url('/partner') }}">Mon Futur partenaire</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<!-- information générale -->
							<h5>Information Générale:</h5>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Nom : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->nom.' '.$profile->prenom}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Adresse : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->adresse}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Âge : </label>
									<div class="col-sm-6 w3_details">
										{{Date::make($profile->age)->until()}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Poids : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->poids}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Taille : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->taille}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Teint : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->teint}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Profession : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->profession}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Sexe : </label>
									<div class="col-sm-6 w3_details">
										{{$a = ($profile->sexe == 'm') ? 'Homme' : 'Femme'}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Nationalité : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->nationalite}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Pays : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->pays}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Réligion : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->religion}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Statut Matrimoniale : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->situation}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Profile ID : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->profile}}
									</div>
									<div class="clearfix"> </div>
								</div>
						</div>
						<div class="col-sm-6 pull-right">
							<!-- détail de la personne désiré -->
							<h5>Partenaire désiré :</h5>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Age : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->intervalle ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Sexe : </label>
									<div class="col-sm-6 w3_details">
										{{ '' ?? ($a = ($profile->user->partner->sexe == "m" ) ? "Homme" : "Femme" ) }}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Taille : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->caste  ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Statut matrimoniale : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->marital  ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Religion : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->religion  ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Poids : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->height  ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Profession : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->domaine  ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Teint : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->teint  ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="form_but1">
									<label class="col-sm-6 control-label1">Pays : </label>
									<div class="col-sm-6 w3_details">
										{{$profile->user->partner->pays ?? ''}}
									</div>
									<div class="clearfix"> </div>
								</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-3 w3ls-aside">
			<h3>Réchercher par Profile ID:</h3>
			<form action="#" method="get"> 
				<input class="text" type="text" name="profile_id" placeholder="Saisir l'ID du Profile" required="">
				<input type="submit" value="Search">
				<div class="clearfix"></div>
			</form>
			<div class="view_profile">
        	<h3>Profiles Similaires</h3>
        	<ul class="profile_item">
        	  	<a href="#">
	        	   	<li class="profile_item-img">
	        	   	  	<img src="images/p1.jpg" class="img-responsive" alt="">
	        	   	</li>
	        	   	<li class="profile_item-desc">
	        	   	  	<h6>ID : 2458741</h6>
	        	   	  	<p>29 Yrs, 5Ft 5in Christian
					  	MBA/PGDM,
					  	Rs 10 - 15 lac Mark...</p>
	        	   	</li>
	        	   	<div class="clearfix"> </div>
        	  	</a>
           </ul>
       </div>
		</div>
		<div class="clearfix"></div>
		</div>
			<!-- Modal -->
		<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Login to Continue</h4>
			  </div>
			  <div class="modal-body">
				<div class="login-w3ls">
					<form id="signin" action="#" method="post">
						<label>User Name </label>
						<input type="text" name="User Name" placeholder="Username" required="">
						<label>Password</label>
						<input type="password" name="Password" placeholder="Password" required="">	
						<div class="w3ls-loginr"> 
							<input type="checkbox" id="brand" name="checkbox" value="">
							<span> Remember me ?</span> 
							<a href="#">Forgot password ?</a>
						</div>
						<div class="clearfix"> </div>
						<input type="submit" name="submit" value="Login">
						<div class="clearfix"> </div>
						<div class="social-icons">
							<ul>  
								<li><a href="#"><span class="icons"><i class="fa fa-facebook" aria-hidden="true"></i></span><span class="text">Facebook</span></a></li> 
								<li class="twt"><a href="#"><span class="icons"><i class="fa fa-twitter" aria-hidden="true"></i></span><span class="text">Twitter</span></a></li>  
							</ul> 
							<div class="clearfix"> </div>
						</div>	
					</form>
				</div>
			  </div>
			</div>

		  </div>
		</div>
			<!-- //Modal -->
	</div>
	<!-- section assistance -->
	<div class="agile-assisted-service text-center">
		<h4>Service Assistance</h4>
		<p>Our Relationship Managers have helped thousands of members find their life partners.</p>
		<a href="">Voir Plus</a>
	</div>
@stop