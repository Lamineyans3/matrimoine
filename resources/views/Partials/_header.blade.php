<header>
 	<div class="navbar navbar-inverse-blue navbar">
      	<div class="navbar-inner">
	        <div class="container">
	          	@include('partials/_menu')  
	          	<div class="clearfix"> </div>
	        </div>  
      	</div>  
    </div>  
</header>