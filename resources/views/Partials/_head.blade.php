	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" /><!-- bootstrap-CSS -->
	<link href="{{ asset('css/font-awesome.css') }} " rel="stylesheet" type="text/css" media="all" /><!-- Fontawesome-CSS -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script type='text/javascript' src="{{ asset('js/jquery-2.2.3.min.js')}}" ></script>
	<!-- Custom Theme files -->
	<link href="{{ asset('css/menu.css') }} " rel="stylesheet" type="text/css" media="all" /> <!-- menu style --> 
	<!--theme-style-->
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />	
	<!--//theme-style-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/easy-responsive-tabs.css') }}" />

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Match Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />

	<script type="application/x-javascript"> 
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
	</script>
<!--//meta data-->
	<script>
		$(document).ready(function(){
		    $(".dropdown").hover(            
		        function() {
		            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
		            $(this).toggleClass('open');        
		        },
		        function() {
		            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
		            $(this).toggleClass('open');       
		        }
		    );
		});
	</script>	

	<link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}} " />
	<script src="{{ asset('js/jquery-ui.js')}} "></script>
		<script>
		  $(function() {
			$( "#datepicker" ).datepicker();
		 });
		</script>

	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}} ">
