<div class="menu">
	<div class="cd-dropdown-wrapper">
		<a class="cd-dropdown-trigger" href="{{ url('/') }}">{{config('app.name')}}</a>
	</div>  	 
</div>
<div class="pull-right">
	<nav class="navbar nav_bottom" role="navigation"> 
  	<div class="navbar-header nav_2">
      	<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">Menu
        	<span class="sr-only">Toggle navigation</span>
        	<span class="icon-bar"></span>
        	<span class="icon-bar"></span>
        	<span class="icon-bar"></span>
      	</button>
   	</div>  
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            	<li class="active"><a href="{{ url('/') }}">Home</a></li>
        	@guest
        	@else
        		<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                    
        				<li class=""><a href="{{ url('/profile') }}">Modifier Mon profile</a></li>
                    	<li class=""><a href="{{ url('/partner') }}">Completer Mon profile</a></li>
                    	<li class=""><a href="{{ route('profile.show', $profile) }}">Mon Profile</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
        	@endif

<!--             <li><a href="about.html">About</a></li>
            <li><a href="search.html">Search</a></li>
            <li><a href="app.html" target="_blank">Mobile</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Quick Search<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <div class="banner-bottom-login">
				<div class="w3agile_banner_btom_login">
					<form action="#" method="post">
						<div class="w3agile__text w3agile_banner_btom_login_left">
							<h4>I'm looking for a</h4>
							<select id="country" onchange="change_country(this.value)" class="frm-field required">
								<option value="null">Bride</option>
								<option value="null">Groom</option>   
							</select>
						</div>
						<div class="w3agile__text w3agile_banner_btom_login_left1">
							<h4>Aged</h4>
							<select id="country1" onchange="change_country(this.value)" class="frm-field required">
								<option value="null">20</option>
								<option value="null">21</option>   
								<option value="null">22</option>   
								<option value="null">23</option>   
								<option value="null">24</option>   
								<option value="null">25</option>  
								<option value="null">- - -</option>   					
							</select>
							<span>To </span>
							<select id="country2" onchange="change_country(this.value)" class="frm-field required">
								<option value="null">30</option>
								<option value="null">31</option>   
								<option value="null">32</option>   
								<option value="null">33</option>   
								<option value="null">34</option>   
								<option value="null">35</option>  
								<option value="null">- - -</option>   					
							</select>
						</div>
						<div class="w3agile__text w3agile_banner_btom_login_left2">
							<h4>Religion</h4>
							<select id="country3" onchange="change_country(this.value)" class="frm-field required">
								<option value="null">Hindu</option>  
								<option value="null">Muslim</option>   
								<option value="null">Christian</option>   
								<option value="null">Sikh</option>   
								<option value="null">Jain</option>   
								<option value="null">Buddhist</option>
								<option value="null">No Religious Belief</option>   					
							</select>
						</div>
						<div class="w3agile_banner_btom_login_left3">
							<input type="submit" value="Search" />
						</div>
						<div class="clearfix"> </div>
					</form>
				</div>
			</div>
              </ul>
            </li>
            <li class="last"><a href="contact.html">Contacts</a></li>
 -->            
        </ul>
    </div>
</nav>
</div>