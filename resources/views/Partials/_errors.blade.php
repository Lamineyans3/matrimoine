@if(isset($errors) && count($errors) > 0)
	<div class="alert alert-danger alert-dismissable">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
		{{$errors->first()}}<br>
	</div>
@endif